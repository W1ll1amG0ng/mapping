#pragma once

#include "conebuffer.h"

#include <memory>
#include <string>
#include <vector>

class Mapping {
    public:

        Mapping(double minn, 
                double loop_detection_dis, 
                int loop_detection_timer_count, 
                int cone_min_update_count, 
                int reg_range, 
                bool enable_loop_detect, 
                std::string world_frame_id, 
                double min_cone_distance, 
                int cone_filter_time);

        void cone(const fsd_common_msgs::ConeDetections::ConstPtr& msg);
        void cone(const fsd_common_msgs::ConeDetections msg);
        void stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg);
        void refreshMap();

        bool getIsStateInited() {return is_state_inited_;}
        bool getIsLoopClosed() {return is_loop_closed_;}
        fsd_common_msgs::ConeDetections getConemap() {
            fsd_common_msgs::ConeDetections conemap = cone_buffer_->getConemap();
            conemap_size_ = conemap.cone_detections.size();
            return conemap;
        }
        visualization_msgs::MarkerArray getConeMarkerArray(std::string frame_id) {return cone_buffer_->getConeMarkerArray(frame_id);}
        fsd_common_msgs::Map getMap() {return cone_buffer_->getMap();}



    private:

        std::shared_ptr<ConeBuffer> cone_buffer_;

        fsd_common_msgs::ConeDetections conemap_;
        fsd_common_msgs::CarState state_current_, state_init_, state_last_;
        fsd_common_msgs::Map map_;
        std::vector<bool> conebj_;
        std::vector<double> statex, statey;

        bool is_state_inited_;
        bool is_loop_closed_;
        double odom_;
        double start_time_;
        double minn_;                       // min spacing of cone, registration range
        double loop_detection_dis_;
        int loop_detection_timer_count_;
        int loop_detection_timer_;          // negative means inactive, positive means active
        int cone_min_update_count_;         // Min update count of a cone which is supposed in map
        double delta_dis_;                  // diff with current dis and last dis, updated with aging algorithm
        int conemap_size_;                  // Only for debug
        int reg_range_;                     // max range when registrate cones
        bool enable_loop_detect_;
        std::string world_frame_id_;
        double min_cone_distance_;
        int cone_filter_time_;              // run coneFilter every <this val> times
        int cone_run_count_;

        // private methods
        double getDeltaDistance();
        //double getDeltaDistance(const fsd_common_msgs::CarState::ConstPtr& msg);
        double dis(const fsd_common_msgs::Cone a, const fsd_common_msgs::Cone b);
        double dis(const fsd_common_msgs::CarState a, const fsd_common_msgs::CarState b);
        double dis(const fsd_common_msgs::CarState a);
};