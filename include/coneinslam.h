#pragma once

#include "fsd_common_msgs/Cone.h"

#include <geometry_msgs/Point.h>

class ConeInSlam {
    public:
        ConeInSlam(int id);
        void update(fsd_common_msgs::Cone cone);

        int getUpdateCount() {return update_count_;}
        geometry_msgs::Point getPosition() const {return cone_.position;}
        fsd_common_msgs::Cone getCone() {return cone_;}
        void setPosition(double x, double y) {cone_.position.x = x; cone_.position.y = y;}

    private:
        int id_;
        fsd_common_msgs::Cone cone_;
        int update_count_;
};