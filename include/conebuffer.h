#pragma once

#include "coneinslam.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/ConeDetections.h"

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <vector>
#include <string>

class ConeBuffer {

    public:

        ConeBuffer(double max_reg_range, 
                   int min_update_count, 
                   fsd_common_msgs::CarState state_init, 
                   double min_cone_distance);

        void registration(fsd_common_msgs::Cone cone); // Use ICP
        void coneFilter();

        fsd_common_msgs::Map getMap();
        visualization_msgs::MarkerArray getConeMarkerArray(std::string frame_id);
        fsd_common_msgs::ConeDetections getConemap();

    private:

        double max_reg_range_;          // Min spacing of cone, max registration range
        double min_cone_distance_;      // cones with distance less than the val are regarded as reg failed. 
        int min_update_count_;          // Cone will be put in the map if its update count is lager than this val. 
        std::vector<ConeInSlam> buffer_;
        int next_id_;
        fsd_common_msgs::CarState state_init_;

        // private methods
        double dis(const fsd_common_msgs::Cone &a, const ConeInSlam &b);
        double dis(const ConeInSlam &a, const ConeInSlam &b);
        void addCone(fsd_common_msgs::Cone cone) {

            ConeInSlam cone_current(next_id_);
            next_id_++;
            cone_current.update(cone);
            buffer_.push_back(cone_current);

        }

};