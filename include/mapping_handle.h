#pragma once

#include "mapping.h"
#include "fsd_common_msgs/ConeDetections.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/Map.h"

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <string>
#include <memory>

class MappingHandle {
    public:
        MappingHandle();

        void startLiveSlam();

        void coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg);
        void stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg);

        void serialization(fsd_common_msgs::Map map);

    private:
        std::unique_ptr<Mapping> mapping_;

        // sub & pub
        ros::NodeHandle node_;
        ros::Publisher map_markerPub_;
        ros::Publisher map_pub;     // ConeDetections
        ros::Publisher map_pub_;
        ros::Publisher realtime_map_pub_;
        ros::Subscriber cone_sub;
        ros::Subscriber state_sub;

        fsd_common_msgs::CarState state_current, state_init;

        fsd_common_msgs::ConeDetections latest_cone_;

        // Parameters used by Mapping
        double minn_;
        double mincos_;
        bool save_;
        double loop_detection_dis_;
        int loop_detection_timer_count_;
        int cone_min_update_count_;
        int reg_range_;
        std::string map_topic_;
        std::string cone_topic_;
        std::string state_topic_;
        std::string realtime_topic_;
        std::string Visualization_frame_id_;
        std::string world_frame_id_;
        bool enable_loop_detect_;

        void map_sendVisualization();
};