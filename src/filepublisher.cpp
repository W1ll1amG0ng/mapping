#include "fsd_common_msgs/Map.h"

#include <ifstream>
#include <ros/ros.h>

int main(int argc, char **argv) {

    ros::init(argc, argv, "mapping");
    ros::Nodehandle node_("~");
    ros::publisher pub = node_.advertise<fsd_common_msgs::Map>("/estimation/slam/map", 1, true);
    std::ifstream infile;
    infile.open("data/map.txt");
    int size = 0;
    infile >> size;
    uint8_t buffer[size] = {0};
    infile >> buffer;
    ROS_INFO("File read");
    ros::serialization::IStream stream(buffer, size);
    fsd_common_msgs::Map map;
    ros::serialization::deserialize(stream, map);
    pub.Publish(map);
    ROS_INFO("published");

    return 0;

}