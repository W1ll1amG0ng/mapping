#include "coneinslam.h"

ConeInSlam::ConeInSlam(int id) {
    id_ = id;
    this->update_count_ = 0;
}

void ConeInSlam::update(fsd_common_msgs::Cone cone) {

    //  value copy to prevent double free

    cone_.position           =    cone.position;
    cone_.color              =    cone.color;
    cone_.poseConfidence     =    cone.poseConfidence;
    cone_.colorConfidence    =    cone.colorConfidence;

    update_count_++;
}