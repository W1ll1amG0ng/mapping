#include "mapping_handle.h"

int main(int argc, char **argv) {

    ros::init(argc, argv, "mapping");
    MappingHandle handle = MappingHandle();
    handle.startLiveSlam();
    //ros::NodeHandle nh;

    while(ros::ok()) {
        ros::spinOnce();
    }
    return 0;

}