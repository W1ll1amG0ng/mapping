#include "mapping.h"

#include <cmath>
#include <ros/ros.h>
#include <limits>

Mapping::Mapping(double minn, 
                 double loop_detection_dis, 
                 int loop_detection_timer_count, 
                 int cone_min_update_count, 
                 int reg_range, 
                 bool enable_loop_detect, 
                 std::string world_frame_id, 
                 double min_cone_distance, 
                 int cone_filter_time) {

    is_state_inited_ = false;
    is_loop_closed_ = false;
    loop_detection_timer_ = -1;    // set inactive
    minn_ = minn;
    loop_detection_dis_ = loop_detection_dis;
    loop_detection_timer_count_ = loop_detection_timer_count;
    cone_min_update_count_ = cone_min_update_count;
    reg_range_ = reg_range;
    enable_loop_detect_ = enable_loop_detect;
    world_frame_id_ = world_frame_id;
    min_cone_distance_ = min_cone_distance;
    cone_filter_time_ = cone_filter_time;

    delta_dis_ = 0;
    state_current_ = fsd_common_msgs::CarState();
    state_last_ = fsd_common_msgs::CarState();
    conemap_size_ = 0;
    odom_ = 0;
    cone_run_count_ = 0;

}

void Mapping::cone(const fsd_common_msgs::ConeDetections::ConstPtr& msg) {
    cone(*msg);
}

void Mapping::cone(const fsd_common_msgs::ConeDetections msg) {

    if(!is_state_inited_) return;

    cone_run_count_++;

    fsd_common_msgs::ConeDetections cones;
    
    for(auto i : msg.cone_detections) {

        fsd_common_msgs::Cone cone;
        //ROS_INFO("%f, %f", i.position.x, i.position.y);

        if(i.position.x*i.position.x + i.position.y*i.position.y > reg_range_) continue;

        cone.position.x = state_current_.car_state.x + (i.position.x)*cos(state_current_.car_state.theta) - i.position.y*sin(state_current_.car_state.theta);
        cone.position.y = state_current_.car_state.y + i.position.y*cos(state_current_.car_state.theta) + (i.position.x)*sin(state_current_.car_state.theta);
        cone.color.data = i.color.data;
        cone.poseConfidence.data=i.poseConfidence.data;

        cones.cone_detections.push_back(cone); 
        
    }

    for(auto i : cones.cone_detections) {
        double adj_distance = std::numeric_limits<double>::max();
        int adj_cone_id;

        int k = 0;

        for(auto j : cones.cone_detections) {
            k++;
            double dis = (
                (i.position.x - j.position.x) * (i.position.x - j.position.x) +
                (i.position.y - j.position.y) * (i.position.y - j.position.y)
            );
            if(dis < adj_distance && i.color.data == j.color.data) {
                adj_distance = dis;
                adj_cone_id = k;
            }
        }

        /*
        // balance the position of adj cone
        int weight = 1;
        if(adj_distance < minn_*minn_) {
            k = 0;
            for(auto j : cones.cone_detections) {
                k++;
                if(k = adj_cone_id) {
                    j.position.x = (weight * i.position.x + j.position.x) / (weight + 1);
                    j.position.y = (weight * i.position.y + j.position.y) / (weight + 1);
                    break;
                }
            }
        }
        */

        if(i.poseConfidence.data > 0.8) {
            
            // Registration
            cone_buffer_->registration(i);

        }
    }

    if(cone_run_count_ % cone_filter_time_ == 0) cone_buffer_->coneFilter();

    refreshMap();

}

void Mapping::stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg) {

    ros::Time start_time = ros::Time::now();

    ROS_INFO("x: %f, y: %f", msg->car_state.x, msg->car_state.y);

    if(msg->header.frame_id != world_frame_id_) return;
    //if(msg->car_state.x*msg->car_state.x + msg->car_state.y*msg->car_state.y > 1000000) return;

    if(!is_state_inited_) {

        state_init_.car_state.x     = msg->car_state.x + 2.4*cos(msg->car_state.theta);
        state_init_.car_state.y     = msg->car_state.y + 2.4*sin(msg->car_state.theta);
        state_init_.car_state.theta = 0*msg->car_state.theta;

        is_state_inited_ = true;
        cone_buffer_ = std::shared_ptr<ConeBuffer>(new ConeBuffer(minn_, 
                                                                  cone_min_update_count_, 
                                                                  state_init_, 
                                                                  min_cone_distance_));
        std::cout<< "state_init" << std::endl;

        start_time_ = msg->header.stamp.toSec();

    }

    // update last state
    state_last_ = state_current_;

    // raletive state for init
    state_current_.car_state.x = (msg->car_state.x + 2.4*cos(msg->car_state.theta) - state_init_.car_state.x);
    state_current_.car_state.y = (msg->car_state.y + 2.4*sin(msg->car_state.theta) - state_init_.car_state.y);
    state_current_.car_state.theta = msg->car_state.theta - state_init_.car_state.theta;
    state_current_.header = msg->header;
    
    //ROS_INFO("init x: %f, y: %f", state_init_.car_state.x, state_init_.car_state.y);
    //ROS_INFO("current x: %f, y: %f", state_current_.car_state.x, state_current_.car_state.y);

    // update odom
    odom_ += getDeltaDistance();

    //if(ok_) ROS_INFO("odom %f time %f mapsize %d over", accumulated_mileage_, msg->header.stamp.toSec()-start_time_, conemap_.cone_detections.size());
    ROS_INFO("odom %f time %f mapsize %d", odom_, msg->header.stamp.toSec()-start_time_, conemap_size_);



    // loop detection
    if(enable_loop_detect_) {

        double current_state_dis = dis(state_current_);
        double last_state_dis = dis(state_last_);
        delta_dis_ = (delta_dis_ + (current_state_dis - last_state_dis)) / 2;

        ROS_INFO("current dis: %f; last dis: %f, delta: %f", current_state_dis, last_state_dis, delta_dis_);

        if(current_state_dis <= loop_detection_dis_ && odom_ >= 10*loop_detection_dis_) {

            if(delta_dis_ > 0 && loop_detection_timer_ < 0) {
                ROS_INFO("TIMER ACTIVE! ");
                loop_detection_timer_ = 0;
            }else if(delta_dis_ > 0 && loop_detection_timer_ >= 0) {
                loop_detection_timer_++;
            }else if(delta_dis_ < 0 && loop_detection_timer_ >= 0) {
                ROS_INFO("TIMER_INACTIVE! ");
                loop_detection_timer_ = -1;
            }

            if(loop_detection_timer_ >= loop_detection_timer_count_) {
                is_loop_closed_ = true;
            }

        }else {

            if(loop_detection_timer_ >= 0) {
                ROS_INFO("TIMER_INACTIVE! ");
                loop_detection_timer_ = -1;
            }

        }

    }

    // delay calculation
    ros::Time current_time = ros::Time::now();
    double delay = current_time.toSec() - start_time.toSec();
    ROS_INFO("State calculation delay: %f ms", delay*1000);

}



void Mapping::refreshMap() {

    //if(!ok_) return;

    fsd_common_msgs::Map map;
    map = cone_buffer_->getMap();
    map_ = map;
    //std::cout << "Map refreshed. " << "\n";

}



double Mapping::getDeltaDistance() {

    double delta_x = state_current_.car_state.x - state_last_.car_state.x;
    double delta_y = state_current_.car_state.y - state_last_.car_state.y;

    double res = sqrt(delta_x*delta_x + delta_y*delta_y);

    return res;

}



double Mapping::dis(const fsd_common_msgs::Cone a, const fsd_common_msgs::Cone b) {
    return sqrt((a.position.x-b.position.x)*(a.position.x-b.position.x) + (a.position.y-b.position.y)*(a.position.y-b.position.y));
}

double Mapping::dis(const fsd_common_msgs::CarState a, const fsd_common_msgs::CarState b) {
    return sqrt((a.car_state.x - b.car_state.x)*(a.car_state.x - b.car_state.x) + (a.car_state.y - b.car_state.y)*(a.car_state.y - b.car_state.y));
}
double Mapping::dis(const fsd_common_msgs::CarState a) {
    return sqrt(a.car_state.x*a.car_state.x + a.car_state.y*a.car_state.y);
}
