#include "mapping_handle.h"

#include <cmath>
#include <fstream>

MappingHandle::MappingHandle() : node_("~") {

    // Parameters used by Mapping
    if (!node_.param("mapping/dis", minn_, 1.5)) {
        ROS_WARN_STREAM("Did not load mapping/dis. Standard value is: " << minn_);
    }
    if (!node_.param("mapping/angle", mincos_, 60.0)) {
        ROS_WARN_STREAM("Did not load mapping/angle. Standard value is: " << mincos_);
    }
    if (!node_.param("mapping/loop_detection_dis", loop_detection_dis_, 0.5)) {
        ROS_WARN_STREAM("Did not load mapping/loop_detection_dis. Standard value is: " << loop_detection_dis_);
    }
    if (!node_.param<bool>("mapping/save", save_, true)) {
        ROS_WARN_STREAM("Did not load mapping/save. Standard value is: " << save_);
    }
    if (!node_.param<std::string>("mapping/map_topic", map_topic_, "/estimation/slam/map")) {
        ROS_WARN_STREAM("Did not load mapping/map_topic. Standard value is: " << map_topic_);
    }
    if (!node_.param<std::string>("mapping/cone_topic", cone_topic_, "/perception/lidar/cone_side")) {
        ROS_WARN_STREAM("Did not load mapping/cone_topic. Standard value is: " << cone_topic_);
    }
    if (!node_.param<std::string>("mapping/state_topic", state_topic_, "/estimation/slam/state")) {
        ROS_WARN_STREAM("Did not load mapping/state_topic. Standard value is: " << state_topic_);
    }
    if (!node_.param<std::string>("mapping/state_realtime_topic", realtime_topic_, "/estimation/slam/realtime_map")) {
        ROS_WARN_STREAM("Did not load mapping/state_realtime_topic. Standard value is: " << realtime_topic_);
    }
    if (!node_.param<std::string>("mapping/Visualization_frame_id", Visualization_frame_id_, "world")) {
        ROS_WARN_STREAM("Did not load mapping/Visualization_frame_id. Standard value is: " << Visualization_frame_id_);
    }
    if (!node_.param<std::string>("mapping/world_frame_id", world_frame_id_, "world")) {
        ROS_WARN_STREAM("Did not load mapping/Visualization_frame_id. Standard value is: " << world_frame_id_);
    }
    if (!node_.param("mapping/loop_detection_timer_count", loop_detection_timer_count_, 10)) {
        ROS_WARN_STREAM("Did not load mapping/loop_detection_timer_count. Standard value is: " << loop_detection_timer_count_);
    }
    if (!node_.param("mapping/cone_min_update_count", cone_min_update_count_, 3)) {
        ROS_WARN_STREAM("Did not load mapping/cone_min_update_count. Standard value is: " << cone_min_update_count_);
    }
    if (!node_.param("mapping/reg_range", reg_range_, 50)) {
        ROS_WARN_STREAM("Did not load mapping/reg_range. Standard value is: " << reg_range_);
    }
    if (!node_.param("mapping/enable_loop_detect", enable_loop_detect_, true)) {
        ROS_WARN_STREAM("Did not load mapping/enable_loop_detect. Standard value is: " << enable_loop_detect_);
    }

    mapping_ = std::unique_ptr<Mapping>(new Mapping(minn_, 
                                                    loop_detection_dis_, 
                                                    loop_detection_timer_count_, 
                                                    cone_min_update_count_, 
                                                    reg_range_, 
                                                    enable_loop_detect_, 
                                                    world_frame_id_, 
                                                    minn_, 
                                                    1));

}

void MappingHandle::startLiveSlam() {

    map_markerPub_ = node_.advertise<visualization_msgs::MarkerArray>("/mapping/visualization", 1, true);
    map_pub = node_.advertise<fsd_common_msgs::ConeDetections>("/mapping", 1, true);
    map_pub_ = node_.advertise<fsd_common_msgs::Map>(map_topic_, 1, true);
    realtime_map_pub_ = node_.advertise<fsd_common_msgs::ConeDetections>(realtime_topic_, 1, true);
    cone_sub = node_.subscribe<fsd_common_msgs::ConeDetections>(cone_topic_, 1, &MappingHandle::coneCallback, this);
    state_sub = node_.subscribe<fsd_common_msgs::CarState>(state_topic_, 1, &MappingHandle::stateCallback, this);
    std::cout << "startLiveSlam" << '\n';

}

/*
void MappingHandle::coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg) {

    if(!mapping_->getIsStateInited()) {
        std::cout << "!init" << std::endl;
        return;
    }

    ros::Time start_time = ros::Time::now();

    mapping_->cone(msg);
    auto conemap = mapping_->getConemap();
    ROS_INFO("conemap size: %d", conemap.cone_detections.size());
    map_pub.publish(conemap);
    realtime_map_pub_.publish(conemap);
    ROS_INFO("conemap published. ");
    //map_pub_.publish(mapping_->getMap());
    //ROS_INFO("publish map");

    if(enable_loop_detect_) {

        if(mapping_->getIsLoopClosed()) {
            ROS_INFO("Loop closed. ");
            map_pub_.publish(mapping_->getMap());
            ROS_INFO("publish map");
        }

    } else {

        map_pub_.publish(mapping_->getMap());
        ROS_INFO("publish map without loop detect mode");

    }

    //std::cout << mapping_->getOk() << "\n";

    //Visualization
    map_sendVisualization();

    ros::Time current_time = ros::Time::now();
    double delay = current_time.toSec() - start_time.toSec();
    ROS_INFO("Map building delay: %f ms", delay*1000);

}
*/

void MappingHandle::coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg) {

    if(!mapping_->getIsStateInited()) {
        std::cout << "!init" << std::endl;
        return;
    }

    // deep copy
    latest_cone_.header = msg->header;
    latest_cone_.cone_detections = msg->cone_detections;
    ROS_INFO("Cone updated. ");

}

void MappingHandle::stateCallback(const fsd_common_msgs::CarState::ConstPtr& msg) {

    mapping_->stateCallback(msg);

    // activate cone
    if(!mapping_->getIsStateInited()) {
        std::cout << "!init" << std::endl;
        return;
    }

    ros::Time start_time = ros::Time::now();

    mapping_->cone(latest_cone_);

    auto conemap = mapping_->getConemap();
    ROS_INFO("conemap size: %d", conemap.cone_detections.size());
    map_pub.publish(conemap);
    realtime_map_pub_.publish(conemap);
    ROS_INFO("conemap published. ");
    //map_pub_.publish(mapping_->getMap());
    //ROS_INFO("publish map");

    if(enable_loop_detect_) {

        if(mapping_->getIsLoopClosed()) {
            ROS_INFO("Loop closed. ");
            map_pub_.publish(mapping_->getMap());
            ROS_INFO("publish map");
            serialization(mapping_->getMap());
        }

    } else {

        map_pub_.publish(mapping_->getMap());
        ROS_INFO("publish map without loop detect mode");

    }

    //std::cout << mapping_->getOk() << "\n";

    //Visualization
    map_sendVisualization();

    ros::Time current_time = ros::Time::now();
    double delay = current_time.toSec() - start_time.toSec();
    ROS_INFO("Map building delay: %f ms", delay*1000);

}

void MappingHandle::map_sendVisualization() {

    map_markerPub_.publish(mapping_->getConeMarkerArray(Visualization_frame_id_));
    std::cout << "MarkerArray Published. " << std::endl;

}

void MappingHandle::serialization(fsd_common_msgs::Map map) {
    int size = ros::serialization::serializationLength(map);
    uint8_t buffer[size] = {0};
    ros::serialization::OStream stream(buffer, size);
    ros::serialization::serialize(stream, map);
    ROS_INFO("Serialization completed");

    // write to file
    std::ofstream outfile;
    outfile.open("data/map.txt");
    outfile << size;
    outfile << buffer;
    outfile.close();
    ROS_INFO("File is written");

}