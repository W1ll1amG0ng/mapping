#include "conebuffer.h"

#include <geometry_msgs/Point.h>
#include <ros/ros.h>

#include <cmath>
#include <limits>
#include <iostream>

ConeBuffer::ConeBuffer(double max_reg_range, 
                       int min_update_count, 
                       fsd_common_msgs::CarState state_init, 
                       double min_cone_distance) {

    max_reg_range_ = max_reg_range;
    min_update_count_ = min_update_count;
    state_init_ = state_init;
    min_cone_distance_ = min_cone_distance;
    next_id_ = 0;

}



void ConeBuffer::registration(fsd_common_msgs::Cone cone) {

    if(buffer_.empty()) {

        this->addCone(cone);

    } else {

        double min_dis = std::numeric_limits<double>::max();
        std::vector<ConeInSlam>::iterator cone_to_update;
        for(auto i = buffer_.begin(); i != buffer_.end(); i++) {

            double current_dis = this->dis(cone, *i);
            if((current_dis <= max_reg_range_) && (min_dis > current_dis)) {

                min_dis = current_dis;
                cone_to_update = i;
                //i->update(cone);

            }

        }

        if(min_dis == std::numeric_limits<double>::max()) {

            this->addCone(cone);

        } else {

            cone_to_update->update(cone);

        }

    }

}



fsd_common_msgs::Map ConeBuffer::getMap() {

    fsd_common_msgs::Map map;

    for(auto i = buffer_.begin(); i != buffer_.end(); i++) {

        if(i->getUpdateCount() >= min_update_count_) {

            fsd_common_msgs::Cone cone = i->getCone();

            cone.position.x = cone.position.x + state_init_.car_state.x;
            cone.position.y = cone.position.y + state_init_.car_state.y;
            if(cone.color.data == "r") map.cone_red.push_back(cone);
            if(cone.color.data == "b") map.cone_blue.push_back(cone);

        }

    }

    return map;

}


visualization_msgs::MarkerArray ConeBuffer::getConeMarkerArray(std::string frame_id) {

    visualization_msgs::Marker cone_marker;
    visualization_msgs::MarkerArray cone_maker_array;

    //Set the marker type
    cone_marker.type = visualization_msgs::Marker::CUBE;
    cone_marker.action = visualization_msgs::Marker::ADD;

    cone_marker.header.stamp = ros::Time::now();
    cone_marker.header.frame_id = frame_id;
														   
    cone_marker.ns = "slam";

    cone_marker.lifetime = ros::Duration();
    cone_marker.frame_locked = true;

    //Set the color
    cone_marker.color.r = 1.0f;
    cone_marker.color.g = 0.0f;
    cone_marker.color.b = 0.0f;
    cone_marker.color.a = 0.8f;

    cone_marker.scale.x = 0.2;
    cone_marker.scale.y = 0.2;
    cone_marker.scale.z = 0.3;

    // default val to fix warn of rviz
    cone_marker.pose.orientation.x = 0;
    cone_marker.pose.orientation.y = 0;
    cone_marker.pose.orientation.z = 0;
    cone_marker.pose.orientation.w = 1;

    for(auto i = buffer_.begin(); i != buffer_.end(); i++) {

        if(i->getUpdateCount() >= min_update_count_) {

            cone_marker.id++;
            cone_marker.pose.position.x = state_init_.car_state.x + i->getPosition().x;
            cone_marker.pose.position.y = state_init_.car_state.y + i->getPosition().y;

            if(i->getCone().color.data=="r") {

                cone_marker.color.r = 1.0f;
                cone_marker.color.g = 0.0f;
                cone_marker.color.b = 0.0f;

            } else if(i->getCone().color.data=="b") {

                cone_marker.color.r = 0.0f;
                cone_marker.color.g = 0.0f;
                cone_marker.color.b = 1.0f;

            } else {

                cone_marker.color.r = 0.0f;
                cone_marker.color.g = 1.0f;
                cone_marker.color.b = 0.0f;

            }

            cone_maker_array.markers.push_back(cone_marker);

        }

    }

    return cone_maker_array;

}



fsd_common_msgs::ConeDetections ConeBuffer::getConemap() {

    fsd_common_msgs::ConeDetections conemap;
    for(auto i = buffer_.begin(); i != buffer_.end(); i++) {

        if(i->getUpdateCount() >= min_update_count_) {

            conemap.cone_detections.push_back(i->getCone());

        }

    }

    return conemap;

}

void ConeBuffer::coneFilter() {

    /**
     * @brief filter cones too closed
     * 
     */

    ROS_INFO("Start cone filter! ");

    auto buffer_end = buffer_.end();

    for(auto i = buffer_.begin(); i != buffer_end; i++) {

        double min_dis = std::numeric_limits<double>::max();
        std::vector<ConeInSlam>::iterator cone_to_del;
        //std::cout << "buffer size: " << buffer_.size() << std::endl;

        for(auto j = buffer_.begin(); j != buffer_.end(); j++) {

            if(i == j) continue;

            double current_dis = dis(*i, *j);
            if(current_dis < min_dis && current_dis <= min_cone_distance_ && i->getCone().color == j->getCone().color) {
                min_dis = current_dis;
                cone_to_del = j;
            }

        }

        if(min_dis == std::numeric_limits<double>::max()) {
            //std::cout << "normal" << std::endl;
            continue;
        }
        //std::cout << "find" << min_dis << std::endl;

        geometry_msgs::Point i_pos = i->getPosition();
        geometry_msgs::Point other_pos = cone_to_del->getPosition();
        i->setPosition((i_pos.x+other_pos.x)/2, (i_pos.y+other_pos.y)/2);
        buffer_.erase(cone_to_del);
        //std::cout << "erased" << std::endl;

    }
}



double ConeBuffer::dis(const fsd_common_msgs::Cone &a, const ConeInSlam &b) {

    geometry_msgs::Point b_pos = b.getPosition();

    return sqrt((a.position.x-b_pos.x)*(a.position.x-b_pos.x) + (a.position.y-b_pos.y)*(a.position.y-b_pos.y));

}

double ConeBuffer::dis(const ConeInSlam &a, const ConeInSlam &b) {

    geometry_msgs::Point a_pos = a.getPosition();
    geometry_msgs::Point b_pos = b.getPosition();

    return sqrt((a_pos.x-b_pos.x)*(a_pos.x-b_pos.x) + (a_pos.y-b_pos.y)*(a_pos.y-b_pos.y));

}